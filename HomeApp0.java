
public class HomeApp0 {

    public static void main(String[] args) {
        //в аргумент передается размер фигуры
        drawTriangle(5);
        drawCross(7);
    }


 /*         *******
            **   **
            * * * *
            *  *  *
            * * * *
            **   **
            ******* */
    /**
     * @param size размер фигуры
     */
    private static void drawCross(int size){
        String star="*";
        String space=" ";

        int middleSpaces=size-4;
        for (int row=0;row<size;row++){
            if(row>size/2){//нижняя половина фигуры
                if(row==size-1){
                    int tmpSize=size;
                    while (tmpSize>0){
                        System.out.print(star);
                        tmpSize--;
                    }
                    return;
                }
                int sideSpaces=row-1;
                String tmpSides=star;
                String spaces="";
                while (sideSpaces>0){
                    spaces+=space;
                    sideSpaces--;
                }
                tmpSides+=spaces;
                int tmpSideSpaces=size-tmpSides.length()-1;
                int tmpMiddle=tmpSides.length()-1-tmpSideSpaces;

                String bottomSides=star;

                while (tmpSideSpaces-1>0){
                    bottomSides+=space;
                    tmpSideSpaces--;
                }
                bottomSides+=star;
                System.out.print(bottomSides);
                while (tmpMiddle>0){
                    System.out.print(space);
                    tmpMiddle--;
                }
                System.out.print(bottomSides);
                System.out.println();
            }else{//верхняя половина фигуры
                if(row!=0){
                    int sideSpaces=row-1;//кол-во пробелов с одной стороны
                    int tmpMiddleSpaces=middleSpaces-(2*sideSpaces);
                    String tmpSides=star;
                    while (sideSpaces>0){
                        tmpSides+=space;
                        sideSpaces--;
                    }
                    tmpSides+=star;

                    System.out.print(tmpSides);
                    //центральные пробелы
                    while (tmpMiddleSpaces>0){
                        System.out.print(space);
                        tmpMiddleSpaces--;
                    }
                    //центральная строка
                    if(row==size/2){
                        sideSpaces=tmpSides.length()-2;//учитывает что центральная звезда одна
                        while (sideSpaces>0){
                            System.out.print(space);
                            sideSpaces--;
                        }
                        System.out.print(star);
                    }else
                        System.out.print(tmpSides);
                }
                else{
                    int tmpSize=size;
                    while (tmpSize>0){
                        System.out.print(star);
                        tmpSize--;
                    }
                }
                System.out.println();
            }
        }
    }




    /*      *
            **
            * *
            *  *
            *****  */
    /**
     *
     *  @param size размер фигуры
     */
    private static void drawTriangle(int size){
        String star="*";
        String space=" ";

        for (int row=0;row<size;row++){
            int outSpace=size-row-1;//наружнии отступы слева от фигуры
            while (outSpace>0){
                System.out.print(space);
                outSpace--;
            }

            //если не первые две и не последняя строки то входи ,- прорисуй отступы внутри фигуры
            if(row>1 && row!=(size-1)){
                System.out.print(star);
                for (int innerSpace = 0; innerSpace < row-1;innerSpace++){
                    System.out.print(space);
                }
                System.out.print(star);
            }else{//сплошное заполнение звездами
                int temp=row+1;
                do{
                    System.out.print(star);
                    temp--;
                }while (temp>0);
            }
            System.out.println();
        }
    }
}
